# tdaq-container

Scripts for a painless TDAQ 10 container.

## Setting up

- pull up the TDAQ 10.1.0 image and create a temporary container
- copy the /sw directory (~12 Gb !) to local disk

```shell
cp_sw.sh
```

- build slim TDAQ container with /sw removed

```shell
docker compose build
```

## Start partition and run web_is server


- spin up headless TDAQ container by running `run_webis.sh` on startup:
  - setup TDAQ environment
  - start partitions 'initial' and 'test'
  - run `webis_server`

```shell
docker compose up
```

Once the partition is up:

- navigate to http://localhost:5000/tdaq/web_is
- see also [webis_server repo](https://gitlab.cern.ch/atlas-tdaq-software/webis_server)

## Run interactively

(/!\ under construction /!\ )

```shell
docker compose up -d bash ./setup_tdaq.sh
```

check the logs:

```shell
docker compose logs --tail
```

Once the partition is up open a container shell:

```shell
docker compose exec tdaq bash
```
