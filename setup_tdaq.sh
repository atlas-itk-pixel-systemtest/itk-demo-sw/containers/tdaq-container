#!/usr/bin/env bash

source /sw/atlas/tdaq/tdaq-10-01-00/installed/setup.sh
# or with cmake for development:
# . /sw/atlas/tdaq-common/tdaq-common-10-01-00/installed/share/cmake_tdaq/cm_setup tdaq-10-01-00
export PATH="$ROOTSYS/bin:$PATH"
# libgl2ps
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/atlas/sw/lcg/releases/gl2ps/1.4.0-2afdb/x86_64-centos7-gcc11-opt/lib
export TDAQ_IPC_INIT_REF=file:/tmp/init.ref
pm_part_hlt.py -p test
setup_daq -ng test.data.xml initial
setup_daq -ng test.data.xml test
