FROM registry.cern.ch/atlas-tdaq/tdaq:10.1.0 as tdaq10

RUN /opt/ayum/ayum install -y LCG_103_pyasn1_*x86_64_centos7_gcc11_opt && \
    git clone https://gitlab.cern.ch/atlas-tdaq-software/webis.git && \
    rm -rf /sw

FROM scratch
WORKDIR /
COPY --from=tdaq10 . .
COPY setup_tdaq.sh .
COPY run_webis.sh .

