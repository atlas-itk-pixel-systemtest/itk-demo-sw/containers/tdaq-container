#!/usr/bin/env bash

id=$(docker create registry.cern.ch/atlas-tdaq/tdaq:10.1.0)
docker cp tdaq:/sw .
docker rm -v $id